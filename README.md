# Ansible Collection - redhatautomation.network

## Installation and Usage

### Installing the Collection

Before using the site_config collection, you need to install it.
You can also include it in a `requirements.yml` file and install it via `ansible-galaxy collection install -r requirements.yml`, using the format:

```yaml
---
collections:
  - name: https://gitlab.com/redhatautomation/network.git
    type: git
```

### Using the collection
```yaml
---
- name: Run Network Function
  hosts: all
  gather_facts: no

  tasks:
    - name: Backup network devices
      ansible.builtin.include_role:
        name: redhatautomation.network.network_backup

    - name: Generate network report
      ansible.builtin.include_role:
        name: redhatautomation.network.network_report

    - name: Parse Network Commands
      ansible.builtin.include_role:
        name: redhatautomation.network.network_parser
      vars:
        run_command: "show interfaces"
```
